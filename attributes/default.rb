#
# Cookbook Name::anbiru
# Attributes::default
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Users that are required for services
default['anbiru']['users']['www'] = {
  home: '/app',
  shell: '/bin/false',
  comment: 'WWW user for nginx/php-fpm',
  system: true,
  manage_home: true
}

default['anbiru']['timezone'] = node.normal['setup']['environment'] == 'production' ? 'Australia/Sydney' : 'Australia/Adelaide'
default['anbiru']['swap_instances'] = %w(t2.micro t2.small)
default['anbiru']['swap_size'] = 2048

default['anbiru']['do_apt_get_update'] = true
default['anbiru']['do_apt_get_upgrade'] = true
default['anbiru']['do_apt_get_dist_upgrade'] = false
default['anbiru']['services_to_disable'] = [] # ['apparmor']
default['anbiru']['disable_byobu'] = true
default['anbiru']['purge_packages'] = %w(puppet screen popularity-contest byobu httpd)
default['anbiru']['install_packages'] = %w(tmux htop wget rsync git)

# motd shit
default['motd']['color'] = true

# OS Hardening
default['os-hardening']['network']['ipv6']['enable'] = true
default['os-hardening']['env']['extra_user_paths'] = []
default['os-hardening']['auth']['retries'] = 3
default['os-hardening']['security']['kernel']['enable_core_dump'] = false
default['os-hardening']['security']['packages']['list'] = ['xinetd', 'inetd', 'ypserv', 'telnet-server', 'rsh-server']
default['os-hardening']['security']['kernel']['enable_module_loading'] = false

# rubocop:disable all
if node.attribute?('vagrant')
  default['ssh']['listen_to'] = ['10.0.1.2']
elsif node.attribute?('ec2')
  default['ssh']['listen_to'] = [node['ec2']['public_ipv4']]
else
  default['ssh']['listen_to'] = ['0.0.0.0']
end
# rubocop:enable all
