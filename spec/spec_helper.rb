require 'chefspec'
require 'chefspec/berkshelf'
require 'simplecov'
require 'codecov'

SimpleCov.start

if ENV['CI'] == 'true'
  require 'codecov'
  SimpleCov.formatter = SimpleCov::Formatter::Codecov
else
  SimpleCov.formatter = SimpleCov::Formatter::HTMLFormatter
end

RSpec.configure do |config|
  config.expect_with(:rspec) { |e| e.syntax = :expect }
  # Specify the path for Chef Solo file cache path (default: nil)
  config.file_cache_path = Chef::Config[:file_cache_path]
  # Specify the Chef log_level (default: :warn)
  # config.log_level = :debug
  config.log_level = :warn
end

at_exit { ChefSpec::Coverage.report! }
