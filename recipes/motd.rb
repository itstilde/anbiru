# Remove ubuntu defaults
%w(00-header 10-help-text 51-cloudguest 90-updates-available 91-release-upgrade 97-overlayroot 98-fsck-at-reboot 98-reboot-required).each do |motd|
  # rubocop:disable all
  motd "#{motd}" do
    # rubocop:enable all
    action :delete
  end

  file "/etc/update-motd.d/#{motd}" do
    action :delete
    only_if { ::File.exist?("/etc/update-motd.d/#{motd}") }
  end
end

# Remove Opsworks static
%w(legal motd.opsworks-static).each do |motd_file|
  file "/etc/#{motd_file}" do
    action :delete
    only_if { ::File.exist?("/etc/#{motd_file}") }
  end
end

motd 'motd' do
  cookbook 'anbiru'
  source   'motd.erb'
  # rubocop:disable all
  variables ({ :stack => 'Stack name', :layer => 'Layer Name', :ipaddress => 'IP ADDRESS' })
  # rubocop:enable all
end
