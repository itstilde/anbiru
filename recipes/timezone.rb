# Timezone symlink file to force a new symlink on the /etc/localtime file.
execute 'set timezone' do
  command "ln -sf /usr/share/zoneinfo/#{node['time']['timezone']} /etc/localtime"
end
