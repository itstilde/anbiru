# Update apt cache list
if node['anbiru']['do_apt_get_update']
  Chef::Log.info('Performing apt-get update')
  execute 'apt-get update' do
    command 'apt-get update'
    ignore_failure true
    action :run
  end
end
# Do dist upgrade else, regular upgrade
if node['anbiru']['do_apt_get_dist_upgrade']
  Chef::Log.info('Performing apt-get dist-upgrade')
  execute 'apt-get dist-upgrade' do
    command 'apt-get dist-upgrade -y'
    ignore_failure true
    action :run
  end
elsif node['anbiru']['do_apt_get_upgrade']
  Chef::Log.info('Performing apt-get upgrade')
  execute 'apt-get upgrade' do
    command 'apt-get upgrade -y'
    ignore_failure true
    action :run
  end
end

# TODO: Check if there's a reboot and slack alert if need be

# Disable services
node['anbiru']['services_to_disable'].each do |svc|
  Chef::Log.info("Disaling service #{svc}")
  service svc do
    action [:stop, :disable]
    ignore_failure true
  end
end

# turn off byobu
file '/etc/profile.d/Z98-byobu' do
  action :delete
  only_if do
    node['anbiru']['disable_byobu'] && ::File.exist?('/etc/profile.d/Z98-byobu')
  end
end

# Purge packages
node['anbiru']['purge_packages'].each do |pkg|
  Chef::Log.info("Purging package: #{pkg}")
  package pkg do
    action :purge
    ignore_failure true
  end
end

# Install extra packages
node['anbiru']['install_packages'].each do |pkg|
  Chef::Log.info("Installing package: #{pkg}")
  package pkg do
    action :install
  end
end

# Clean up
Chef::Log.info('Performing apt cleanup')
execute 'apt-get autoremove' do
  command 'apt-get autoremove'
  ignore_failure true
  action :run
end
