# Setting up users
# https://docs.chef.io/resource_user.html
# http://jtimberman.housepub.org/blog/2013/09/10/managing-secrets-with-chef-vault/
#
# Each of these users has a data bag with their setup options in database/users
# create a group for this user so users can be in other users groups

Chef::Log.info 'Creating Users!'
node['anbiru']['users'].each do |group, _options|
  Chef::Log.info "Creating group #{group}"
  # rubocop:disable all
  group "#{group}" do
    action :create
    not_if "getent group #{group}"
  end
  # rubocop:enable all
end

node['anbiru']['users'].each do |user, options|
  Chef::Log.info "Creating user #{user}"
  # rubocop:disable all
  user "#{user}" do
    group user.to_s
    system options['system']
    shell options['shell']
    home options['home']
    comment options['comment']
    shell options['shell']
    action :create
    not_if "getent passwd #{user}"
  end
  # rubocop:enable all
end
