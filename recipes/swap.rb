# Sets a swap sizeeee
if ::File.exist?('/var/swapfile')
  Chef::Log.warn('Swapfile from opsworks exists, trying to remove')
  swap_file '/var/swapfile' do
    action :remove
  end
end

# if this instance is in the list of instances to add a swap too
if node.attribute?('vagrant')
  Chef::Log.info("Creating SWAP with size #{node['anbiru']['swap_size']} bytes")
  swap_file '/var/swap' do
    size node['anbiru']['swap_size']
    not_if { ::File.exist?('var/swap') }
  end
elsif node.attribute?('ec2')
  if node['anbiru']['swap_instances'].include?(node['ec2']['instance_type'])
    Chef::Log.info("Creating SWAP with size #{node['anbiru']['swap_size']} bytes")
    swap_file '/var/swap' do
      size node['anbiru']['swap_size']
      not_if { ::File.exist?('var/swap') }
    end
  end
end
