# *A.N.B.I.R.U*
anbiru (anvil) provides a solid set of recipes to provide a secure & based instance
###### _its~_
______

##### Usage




### Platform Support

* ubuntu (>= 16.04)

### Cookbook Dependencies

* motd
* os-hardening
* ssh-hardening
* swap

### Attributes

Attribute | Description | Default | Choices
----------|-------------|---------|--------
`node['anbiru']['timezone']` | `` | "node.normal['setup']['environment'] == 'production' ? 'Australia/Sydney' : 'Australia/Adelaide" |
`node['anbiru']['swap_instances']` | `` | "%w(t2.micro t2.small)" |
`node['anbiru']['swap_size']` | `` | "2048" |
`node['anbiru']['do_apt_get_update']` | `` | "true" |
`node['anbiru']['do_apt_get_upgrade']` | `` | "true" |
`node['anbiru']['do_apt_get_dist_upgrade']` | `` | "false" |
`node['anbiru']['services_to_disable']` | `` | "[ ... ]" |
`node['anbiru']['disable_byobu']` | `` | "true" |
`node['anbiru']['purge_packages']` | `` | "%w(puppet screen popularity-contest byobu httpd)" |
`node['anbiru']['install_packages']` | `` | "%w(tmux htop wget rsync git)" |
`node['motd']['color']` | `` | "true" |
`node['os-hardening']['network']['ipv6']['enable']` | `` | "true" |
`node['os-hardening']['env']['extra_user_paths']` | `` | "[ ... ]" |
`node['os-hardening']['auth']['retries']` | `` | "3" |
`node['os-hardening']['security']['kernel']['enable_core_dump']` | `` | "false" |
`node['os-hardening']['security']['packages']['list']` | `` | "[ ... ]" |
`node['os-hardening']['security']['kernel']['enable_module_loading']` | `` | "false" |
`node['ssh']['listen_to']` | `` | "[ ... ]" |

### Recipes

* anbiru::default - Does all of the things
* anbiru::timezone - Sets up the time zone
* anbiru::swap - Makes swap space ya dingus
* anbiru::users - Sets up users for things we need done
* anbiru::motd - Sets up a nice MOTD
* anbiru::bash - Sets up bash skel files & whatnot
* anbiru::apt - Updates everything and fixes some annoyances
* anbiru::kernel - Performs some kernel optimization tweaks

### Development and Testing

### Requirements

You will need [Ruby] with [Bundler].

[VirtualBox] and [Vagrant] are required
for integration testing with [Test Kitchen].

*Todo* Write a onliner for installing these via brew



### Rake

Run `rake list` for a list of commands


### Thor

Run `thor -T` to see all Thor tasks.



## License

MIT License

Copyright (c) 2016 its~

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


